package eu.verante.trader.service.management;

import eu.verante.trader.domain.management.UpdateCandlesRequest;
import eu.verante.trader.domain.management.UpdateSymbolsRequest;
import eu.verante.trader.util.asyncprocess.AsyncProcess;
import eu.verante.trader.util.asyncprocess.AsyncProcessHandler;
import eu.verante.trader.util.asyncprocess.AsyncProcessManager;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class TraderManagementService {

    private final Log logger = LogFactory.getLog(TraderManagementService.class);

    @Autowired
    private AsyncProcessManager processManager;

    @Autowired
    private AsyncProcessHandler<UpdateSymbolsRequest> updateSymbolsHandler;

    @Autowired
    private AsyncProcessHandler<UpdateCandlesRequest> updateCandlesHandler;


    public AsyncProcess updateSymbols(UpdateSymbolsRequest request){
        logger.info("Updating symbols with request: " + request);
        return processManager.runProcess(request, updateSymbolsHandler);
    }

    public AsyncProcess updateCandles(UpdateCandlesRequest request) {
        logger.info("Updating candles with request: " + request);
        return processManager.runProcess(request, updateCandlesHandler);
    }

    public AsyncProcess getProcessStatus(UUID id) {
        return processManager.getById(id);
    }
}
