package eu.verante.trader.service;

import eu.verante.trader.stock.candle.TradingCandle;
import eu.verante.trader.stock.symbol.TradingSymbol;
import eu.verante.trader.model.entity.TradingCandleEntity;
import eu.verante.trader.model.entity.TradingCandleSummaryView;
import eu.verante.trader.model.entity.TradingSymbolEntity;
import eu.verante.trader.domain.candle.TradingCandleSummary;
import eu.verante.trader.domain.symbol.TradingSymbolQuery;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface EntityMapper {

    void toSymbolEntity(TradingSymbol symbol, @MappingTarget TradingSymbolEntity entity);
    TradingSymbolEntity toSymbolEntity(TradingSymbolQuery query);
    TradingSymbol toSymbol(TradingSymbolEntity entity);
    void toCandleEntity(TradingCandle candle, @MappingTarget TradingCandleEntity entity);

    TradingCandle toCandle(TradingCandleEntity entity);
    TradingCandleSummary toSummary(TradingCandleSummaryView view);
}
