package eu.verante.trader.service.management;

import eu.verante.trader.broker.BrokerClient;
import eu.verante.trader.stock.candle.Period;
import eu.verante.trader.stock.candle.TradingCandle;
import eu.verante.trader.domain.management.UpdateCandlesRequest;
import eu.verante.trader.domain.management.UpdateStrategy;
import eu.verante.trader.model.entity.TradingCandleEntity;
import eu.verante.trader.model.entity.TradingCandleId;
import eu.verante.trader.model.repository.TradingCandleRepository;
import eu.verante.trader.service.EntityMapper;
import eu.verante.trader.util.asyncprocess.AbstractAsyncProcessHandler;
import eu.verante.trader.util.asyncprocess.AsyncProcess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class UpdateCandlesHandler extends AbstractAsyncProcessHandler<UpdateCandlesRequest> {

    @Autowired
    private BrokerClient brokerClient;

    @Autowired
    private TradingCandleRepository repository;

    @Autowired
    private EntityMapper mapper;


    private final Map<UpdateStrategy, StrategyExecutor> strategyExecutors;

    interface StrategyExecutor {
        TradingCandleEntity execute(TradingCandle symbol);
    }

    public UpdateCandlesHandler() {
        this.strategyExecutors = Map.of(
                UpdateStrategy.REFRESH, this::processRefresh,
                UpdateStrategy.ADD_NEW, this::processAddNew);
    }

    @Override
    protected void doProcess(UpdateCandlesRequest request, AsyncProcess process) {
        int processCount = request.getSymbols().size() * request.getPeriods().size();
        process.setTotal(processCount);

        request.getSymbols().forEach(symbol -> processSymbol(symbol, request, process));
    }

    private void processSymbol(String symbol, UpdateCandlesRequest request, AsyncProcess process) {
        request.getPeriods().forEach(period -> processPeriod(symbol, period, request, process));
    }

    private void processPeriod(String symbol, Period period, UpdateCandlesRequest request, AsyncProcess process) {
        List<TradingCandle> candles = brokerClient.getCandles(symbol, period, request.getFrom().toInstant().toEpochMilli());

        process.setTotal(candles.size());
        List<TradingCandleEntity> entities = candles.stream()
                .map(candle -> processCandle(candle, request.getStrategy(), process))
                .collect(Collectors.toList());
        repository.saveAll(entities);
    }

    private TradingCandleEntity processCandle(TradingCandle candle, UpdateStrategy strategy, AsyncProcess process) {
        TradingCandleEntity entity = strategyExecutors.get(strategy).execute(candle);
        process.setProgress(process.getProgress()+1);
        return entity;
    }


    private TradingCandleEntity processAddNew(TradingCandle tradingCandle) {
        Optional<TradingCandleEntity> existingEntity = repository.findById(generateId(tradingCandle));
        if (existingEntity.isEmpty()) {
            TradingCandleEntity newEntity = new TradingCandleEntity();
            mapper.toCandleEntity(tradingCandle, newEntity);
            return newEntity;
        }
        return existingEntity.get();
    }


    private TradingCandleEntity processRefresh(TradingCandle tradingCandle) {
        TradingCandleEntity entity = repository.findById(
                        generateId(tradingCandle))
                .orElse(new TradingCandleEntity());
        mapper.toCandleEntity(tradingCandle, entity);
        return entity;
    }

    private static TradingCandleId generateId(TradingCandle tradingCandle) {
        return new TradingCandleId(tradingCandle.symbol(), toDomainPeriod(tradingCandle), tradingCandle.time());
    }

    private static eu.verante.trader.model.entity.Period toDomainPeriod(TradingCandle tradingCandle) {
        return eu.verante.trader.model.entity.Period.valueOf(tradingCandle.period().name());
    }
}
