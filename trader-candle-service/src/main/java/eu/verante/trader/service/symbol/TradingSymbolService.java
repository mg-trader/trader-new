package eu.verante.trader.service.symbol;

import eu.verante.trader.stock.symbol.TradingSymbol;
import eu.verante.trader.domain.symbol.TradingSymbolQuery;
import eu.verante.trader.model.entity.TradingSymbolEntity;
import eu.verante.trader.model.repository.TradingSymbolRepository;
import eu.verante.trader.service.EntityMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class TradingSymbolService {
    @Autowired
    private TradingSymbolRepository repository;

    @Autowired
    private EntityMapper mapper;
    public List<TradingSymbol> getSymbols(TradingSymbolQuery query) {
        TradingSymbolEntity exampleEntity = mapper.toSymbolEntity(query);
        return repository.findAll(Example.of(exampleEntity))
                .stream()
                .map(mapper::toSymbol)
                .collect(Collectors.toList());
    }

    public TradingSymbol getSymbol(String symbol) {
        return repository.findById(symbol)
                .map(mapper::toSymbol)
                .orElseThrow();
    }
}
