package eu.verante.trader.service.management;

import eu.verante.trader.broker.BrokerClient;
import eu.verante.trader.domain.management.UpdateStrategy;
import eu.verante.trader.domain.management.UpdateSymbolsRequest;
import eu.verante.trader.stock.symbol.TradingSymbol;
import eu.verante.trader.model.entity.TradingSymbolEntity;
import eu.verante.trader.model.repository.TradingSymbolRepository;
import eu.verante.trader.service.EntityMapper;
import eu.verante.trader.util.asyncprocess.AbstractAsyncProcessHandler;
import eu.verante.trader.util.asyncprocess.AsyncProcess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class UpdateSymbolsHandler extends AbstractAsyncProcessHandler<UpdateSymbolsRequest> {

    @Autowired
    private BrokerClient brokerClient;

    @Autowired
    private TradingSymbolRepository repository;

    @Autowired
    private EntityMapper mapper;

    private final Map<UpdateStrategy, StrategyExecutor> strategyExecutors;

    interface StrategyExecutor {
        TradingSymbolEntity execute(TradingSymbol symbol);
    }

    public UpdateSymbolsHandler() {
        this.strategyExecutors = Map.of(
                UpdateStrategy.REFRESH, this::processRefresh,
                UpdateStrategy.ADD_NEW, this::processAddNew);
    }

    @Override
    protected void doProcess(UpdateSymbolsRequest request, AsyncProcess process) {
        List<TradingSymbol> symbols = brokerClient.getTradingSymbols(
                request.getSymbol(), request.getCategory(), request.getGroupName());
        process.setTotal(symbols.size()+1);
        List<TradingSymbolEntity> entities = symbols.stream()
                .map(symbol -> processSymbol(symbol, request.getStrategy(), process))
                .collect(Collectors.toList());
        repository.saveAll(entities);
    }

    private TradingSymbolEntity processSymbol(TradingSymbol symbol, UpdateStrategy strategy, AsyncProcess process) {
        TradingSymbolEntity entity = strategyExecutors.get(strategy).execute(symbol);
        process.setProgress(process.getProgress()+1);
        return entity;
    }

    private TradingSymbolEntity processRefresh(TradingSymbol symbol) {
        TradingSymbolEntity entity = repository.findById(symbol.symbol())
                .orElse(new TradingSymbolEntity());
        mapper.toSymbolEntity(symbol, entity);
        return entity;
    }

    private TradingSymbolEntity processAddNew(TradingSymbol symbol) {
        Optional<TradingSymbolEntity> existingEntity = repository.findById(symbol.symbol());
        if (existingEntity.isEmpty()) {
            TradingSymbolEntity newEntity = new TradingSymbolEntity();
            mapper.toSymbolEntity(symbol, newEntity);
            return newEntity;
        }
        return existingEntity.get();
    }
}
