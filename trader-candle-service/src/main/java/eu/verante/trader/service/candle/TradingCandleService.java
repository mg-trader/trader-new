package eu.verante.trader.service.candle;

import eu.verante.trader.stock.candle.TradingCandle;
import eu.verante.trader.domain.candle.TradingCandleQuery;
import eu.verante.trader.domain.candle.TradingCandleSummary;
import eu.verante.trader.model.entity.Period;
import eu.verante.trader.model.entity.TradingCandleEntity;
import eu.verante.trader.model.entity.TradingCandleSummaryView;
import eu.verante.trader.model.repository.TradingCandleRepository;
import eu.verante.trader.service.EntityMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static eu.verante.trader.model.entity.TradingCandleEntity_.*;
import static org.springframework.data.jpa.domain.Specification.allOf;

@Component
public class TradingCandleService {

    @Autowired
    private TradingCandleRepository repository;

    @Autowired
    private EntityMapper mapper;

    public List<TradingCandle> getCandles(String symbol, eu.verante.trader.stock.candle.Period period, TradingCandleQuery query) {
        List<Specification<TradingCandleEntity>> filters = new ArrayList<>();

        filters.add(equal(SYMBOL, symbol));
        filters.add(equal(PERIOD, Period.valueOf(period.name())));
        if (query.getFrom() != null) {
            query.getFromOptional().ifPresent(from -> filters.add(greaterOrEqual(TIME, from)));
        }
        if (query.getTo() != null) {
            query.getToOptional().ifPresent(to -> filters.add(lessOrEqual(TIME, to)));
        }

        return repository.findAll(allOf(filters))
                .stream()
                .map(mapper::toCandle)
                .collect(Collectors.toList());
    }

    public TradingCandleSummary getCandlesSummary(String symbol, eu.verante.trader.stock.candle.Period period) {
        TradingCandleSummaryView summary = repository.getCandleStats(symbol, Period.valueOf(period.name()));
        return mapper.toSummary(summary);
    }

    private static Specification<TradingCandleEntity> equal(String attribute, Object value) {
        return (root, query1, criteriaBuilder) -> criteriaBuilder.equal(root.get(attribute), value);
    }

    private static <Y extends Comparable<? super Y>> Specification<TradingCandleEntity> greaterOrEqual(String attribute, Y value) {
        return (root, query1, criteriaBuilder) ->
                criteriaBuilder.greaterThanOrEqualTo(root.get(attribute), value);
    }

    private static <Y extends Comparable<? super Y>> Specification<TradingCandleEntity> lessOrEqual(String attribute, Y value) {
        return (root, query1, criteriaBuilder) ->
                criteriaBuilder.lessThanOrEqualTo(root.get(attribute), value);
    }


}
