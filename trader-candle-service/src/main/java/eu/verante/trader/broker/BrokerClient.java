package eu.verante.trader.broker;

import eu.verante.trader.stock.broker.BrokerResource;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "broker", url = "${clients.broker.url}")
public interface BrokerClient extends BrokerResource {

}
