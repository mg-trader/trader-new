package eu.verante.trader.api.candle;

import eu.verante.trader.domain.candle.*;
import eu.verante.trader.service.candle.TradingCandleService;
import eu.verante.trader.stock.candle.Period;
import eu.verante.trader.stock.candle.TradingCandle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/symbol")
public class TradingCandleController implements TradingCandleResource {

    @Autowired
    private TradingCandleService service;

    @Override
    public List<TradingCandle> getCandles(String symbol, Period period,
                                          TradingCandleQuery query) {
        return service.getCandles(symbol, period, query);
    }

    @Override
    public TradingCandleSummary getCandleSummary(String symbol, Period period) {
        return service.getCandlesSummary(symbol, period);
    }
}
