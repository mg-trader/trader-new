package eu.verante.trader.api.management;

import eu.verante.trader.domain.management.TraderManagementResource;
import eu.verante.trader.domain.management.UpdateCandlesRequest;
import eu.verante.trader.domain.management.UpdateSymbolsRequest;
import eu.verante.trader.service.management.TraderManagementService;
import eu.verante.trader.util.asyncprocess.AsyncProcess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping("/management")
public class TraderManagementController implements TraderManagementResource {

    @Autowired
    private TraderManagementService managementService;

    @Override
    public AsyncProcess updateSymbols(UpdateSymbolsRequest request){
        return managementService.updateSymbols(request);
    }

    @Override
    public AsyncProcess updateCandles(UpdateCandlesRequest request) {
        return managementService.updateCandles(request);
    }

    @Override
    public AsyncProcess getProcessStatus(UUID processId) {
        return managementService.getProcessStatus(processId);
    }

}
