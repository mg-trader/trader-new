package eu.verante.trader.api.symbol;

import eu.verante.trader.stock.symbol.TradingSymbol;
import eu.verante.trader.domain.symbol.TradingSymbolQuery;
import eu.verante.trader.domain.symbol.TradingSymbolResource;
import eu.verante.trader.service.symbol.TradingSymbolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/symbol")
public class TradingSymbolController implements TradingSymbolResource {

    @Autowired
    private TradingSymbolService symbolService;

    @Override
    public List<TradingSymbol> getSymbols(TradingSymbolQuery query) {
        return symbolService.getSymbols(query);
    }

    @Override
    public TradingSymbol getSymbol(String symbol) {
        return symbolService.getSymbol(symbol);
    }
}
