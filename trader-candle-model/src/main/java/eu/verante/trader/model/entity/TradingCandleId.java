package eu.verante.trader.model.entity;

import lombok.*;
import org.springframework.data.annotation.AccessType;

import java.io.Serializable;
import java.time.ZoneId;
import java.time.ZonedDateTime;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@AccessType(AccessType.Type.PROPERTY)
public class TradingCandleId implements Serializable {

    @Setter
    private String symbol;
    @Setter
    private Period period;
    @EqualsAndHashCode.Exclude
    private ZonedDateTime time;

    @EqualsAndHashCode.Include
    public long getTimeMillis() {
        return getTime().toInstant().toEpochMilli();
    }

    public void setTime(ZonedDateTime time) {
        this.time = time.withZoneSameInstant(ZoneId.systemDefault());
    }
}
