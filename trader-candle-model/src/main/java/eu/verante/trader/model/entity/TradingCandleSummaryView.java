package eu.verante.trader.model.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.ZonedDateTime;

@Getter
@AllArgsConstructor
public class TradingCandleSummaryView {

    private ZonedDateTime fromTime;
    private ZonedDateTime toTime;
}
