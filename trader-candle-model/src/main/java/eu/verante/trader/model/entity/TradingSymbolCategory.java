package eu.verante.trader.model.entity;

public enum TradingSymbolCategory {

    STOCK,
    INDEX,
    FOREX,
    COMMODITY,
    ETF,
    CRYPTO
}
