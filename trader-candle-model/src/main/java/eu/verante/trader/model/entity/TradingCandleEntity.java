package eu.verante.trader.model.entity;

import jakarta.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZoneId;
import java.time.ZonedDateTime;

@Entity(name = "TRADING_CANDLE")
@IdClass(TradingCandleId.class)
@Data
@EqualsAndHashCode(callSuper = true)
public class TradingCandleEntity extends UpdateableEntity implements Serializable {

    @Id
    private String symbol;
    @Id
    @Enumerated(EnumType.STRING)
    private Period period;
    @Id
    @Temporal(TemporalType.TIMESTAMP)
    private ZonedDateTime time;

    BigDecimal open;
    BigDecimal close;
    BigDecimal high;
    BigDecimal low;
    BigDecimal volume;

    public void setTime(ZonedDateTime time) {
        this.time = time.withZoneSameInstant(ZoneId.systemDefault());
    }

}
