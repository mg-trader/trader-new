package eu.verante.trader.model.repository;

import eu.verante.trader.model.entity.TradingSymbolEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface TradingSymbolRepository extends JpaRepository<TradingSymbolEntity, String>,
        JpaSpecificationExecutor<TradingSymbolEntity> {
}
