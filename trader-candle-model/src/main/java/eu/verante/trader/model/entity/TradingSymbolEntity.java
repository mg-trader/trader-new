package eu.verante.trader.model.entity;

import jakarta.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

@Entity(name = "TRADING_SYMBOL")
@Data
@EqualsAndHashCode(callSuper = true)
public class TradingSymbolEntity extends UpdateableEntity implements Serializable {

    @Id
    private String symbol;
    private String description;
    private String currency;
    @Column(name = "GROUP_NAME")
    private String groupName;

    @Enumerated(EnumType.STRING)
    private TradingSymbolCategory category;
}
