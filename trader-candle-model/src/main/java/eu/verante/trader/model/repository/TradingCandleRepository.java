package eu.verante.trader.model.repository;

import eu.verante.trader.model.entity.Period;
import eu.verante.trader.model.entity.TradingCandleEntity;
import eu.verante.trader.model.entity.TradingCandleId;
import eu.verante.trader.model.entity.TradingCandleSummaryView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface TradingCandleRepository extends JpaRepository<TradingCandleEntity, TradingCandleId>,
        JpaSpecificationExecutor<TradingCandleEntity> {

    @Query("select new eu.verante.trader.model.entity.TradingCandleSummaryView(min(c.time), max(c.time)) " +
            "from eu.verante.trader.model.entity.TradingCandleEntity c where c.symbol = :symbol and c.period = :period")
    TradingCandleSummaryView getCandleStats(@Param("symbol") String symbol, @Param("period") Period period);
}
