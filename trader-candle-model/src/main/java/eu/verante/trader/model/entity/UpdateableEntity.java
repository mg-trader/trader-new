package eu.verante.trader.model.entity;

import jakarta.persistence.*;
import lombok.Data;

import java.time.ZonedDateTime;

@MappedSuperclass
@Data
public abstract class UpdateableEntity {

    @Temporal(TemporalType.TIMESTAMP)
    private ZonedDateTime created;

    @Temporal(TemporalType.TIMESTAMP)
    private ZonedDateTime updated;

    @PrePersist
    protected void onCreate() {
        created = ZonedDateTime.now();
        updated = created;
    }

    @PreUpdate
    protected void onUpdate() {
        updated = ZonedDateTime.now();
    }
}
