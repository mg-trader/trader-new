create table TRADING_SYMBOL (
    symbol varchar(20) not null,
    description varchar(256),
    currency varchar(3) not null,
    group_name varchar(20) not null,
    category varchar(10) not null,
    created timestamp with time zone not null,
    updated timestamp with time zone not null,
    CONSTRAINT trading_symbol_pkey PRIMARY KEY (symbol)
);

create table TRADING_CANDLE (
    symbol varchar(10) not null,
    period varchar(10) not null,
    "time" timestamp with time zone not null,
    "open" decimal(15,5) not null,
    "close" decimal(15,5) not null,
    high decimal(15,5) not null,
    low decimal(15,5) not null,
    volume decimal (15,5) not null,
    created timestamp with time zone not null,
    updated timestamp with time zone not null,
    CONSTRAINT trading_candle_pley PRIMARY KEY (symbol, period, "time")
);
