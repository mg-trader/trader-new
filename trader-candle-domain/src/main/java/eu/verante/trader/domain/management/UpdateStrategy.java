package eu.verante.trader.domain.management;

public enum UpdateStrategy {

    ADD_NEW,
    REFRESH
}
