package eu.verante.trader.domain.management;

import eu.verante.trader.stock.symbol.TradingSymbolCategory;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UpdateSymbolsRequest {

    private UpdateStrategy strategy;

    private String symbol;

    private TradingSymbolCategory category;

    private String groupName;
}
