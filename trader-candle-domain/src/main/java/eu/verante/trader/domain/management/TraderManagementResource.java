package eu.verante.trader.domain.management;

import eu.verante.trader.util.asyncprocess.AsyncProcess;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.UUID;

public interface TraderManagementResource {
    @PostMapping("/symbol/update")
    AsyncProcess updateSymbols(@RequestBody UpdateSymbolsRequest request);

    @PostMapping("/candle/update")
    AsyncProcess updateCandles(@RequestBody UpdateCandlesRequest request);

    @GetMapping("/{processId}/status")
    AsyncProcess getProcessStatus(@PathVariable("processId") UUID processId);
}
