package eu.verante.trader.domain.candle;

import eu.verante.trader.stock.candle.Period;
import eu.verante.trader.stock.candle.TradingCandle;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

public interface TradingCandleResource {
    @GetMapping("/{symbol}/candles/{period}")
    List<TradingCandle> getCandles(@PathVariable("symbol") String symbol,
                                   @PathVariable("period") Period period,
                                   @SpringQueryMap TradingCandleQuery query);

    @GetMapping("/{symbol}/candles/{period}/summary")
    TradingCandleSummary getCandleSummary(@PathVariable("symbol") String symbol,
                                          @PathVariable("period") Period period);
}
