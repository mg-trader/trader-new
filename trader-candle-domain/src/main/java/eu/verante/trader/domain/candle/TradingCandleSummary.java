package eu.verante.trader.domain.candle;

import lombok.Data;

import java.time.ZonedDateTime;

@Data
public class TradingCandleSummary {

    private ZonedDateTime fromTime;
    private ZonedDateTime toTime;

}
