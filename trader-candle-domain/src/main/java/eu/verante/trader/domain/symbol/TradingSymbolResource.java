package eu.verante.trader.domain.symbol;

import eu.verante.trader.stock.symbol.TradingSymbol;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

public interface TradingSymbolResource {
    @GetMapping("")
    List<TradingSymbol> getSymbols(@SpringQueryMap TradingSymbolQuery query);

    @GetMapping("/{symbol}")
    TradingSymbol getSymbol(@PathVariable("symbol") String symbol);
}
