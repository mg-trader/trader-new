package eu.verante.trader.domain.candle;

import lombok.Builder;
import lombok.Data;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Optional;

@Data
@Builder
public class TradingCandleQuery {
    private Long from;
    private Long to;

    public Optional<ZonedDateTime> getFromOptional() {
        return Optional.ofNullable(from == null ? null : Instant.ofEpochMilli(from).atZone(ZoneId.systemDefault()));
    }

    public Optional<ZonedDateTime> getToOptional() {
        return Optional.ofNullable(to == null ? null : Instant.ofEpochMilli(to).atZone(ZoneId.systemDefault()));
    }
}
