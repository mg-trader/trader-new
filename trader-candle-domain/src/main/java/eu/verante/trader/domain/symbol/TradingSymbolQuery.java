package eu.verante.trader.domain.symbol;

import eu.verante.trader.stock.symbol.TradingSymbolCategory;
import lombok.Data;

@Data
public class TradingSymbolQuery {

    private String symbol;
    private String currency;
    private String groupName;
    private TradingSymbolCategory category;

}
