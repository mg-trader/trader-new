package eu.verante.trader.domain.management;

import eu.verante.trader.stock.candle.Period;
import lombok.Builder;
import lombok.Data;

import java.time.ZonedDateTime;
import java.util.List;

@Data
@Builder
public class UpdateCandlesRequest {

    private UpdateStrategy strategy;
    private List<String> symbols;
    private List<Period> periods;
    private ZonedDateTime from;
    private ZonedDateTime to;
 }
